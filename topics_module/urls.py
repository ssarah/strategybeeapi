from django.urls import path, re_path

from topics_module import selectors


urlpatterns = [

    path('category/', selectors.CategoryListView.as_view(), name='categories-all'),
    path('category/<str:category_title>/', selectors.FieldListViewSet.as_view(), name='category-Fields'),
    path('category/<str:category_title>/<str:field_title>/', selectors.TopicListViewSet.as_view(), name='Field-Topics'),
    path('category/<str:category_title>/<str:field_title>/<str:topic_title>/mentors/get', selectors.TopicBasedMentorsList.as_view(), name='Topic-Mentors'),
    path('topics', selectors.AllTopicsViewSet.as_view(), name='topics-all'),


]

