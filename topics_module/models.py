from django.db import models


# Category is the parent of Field.
class Category(models.Model):
    title = models.CharField(max_length=50,unique=True,default='')
    created = models.DateField()
    description = models.TextField(max_length=1000)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('title',)


# Field will be the child of Category
class Field(models.Model):
    parentCategory = models.ForeignKey(Category, on_delete=models.CASCADE, default='Test Preparation')
    title = models.CharField(max_length=50,unique=True,default='')
    created = models.DateField()
    description = models.TextField(max_length=1000)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('title',)

# Topic is the child of Field
class Topic(models.Model):
    title = models.CharField(max_length=50,unique=True,default='')
    parentField = models.ForeignKey(Field, on_delete=models.CASCADE, default='Miscellaneous')
    created = models.DateField()
    description = models.TextField(max_length=1000)
    STATUS_CHOICES = (
        ('ACTIVE', 'Active'),
        ('ARCHIVED', 'Archived'),
    )
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='ACTIVE')

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('title',)





