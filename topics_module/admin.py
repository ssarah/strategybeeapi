from django.contrib import admin
from .models import Category, Field, Topic

# Register your models here.
admin.site.register(Category)
admin.site.register(Field)
admin.site.register(Topic)