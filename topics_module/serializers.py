from rest_framework import serializers
from topics_module import models
from new_user_module.serializers import UserMentorSerializer


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Category
        fields = ('id', 'created', 'title', 'description')


class FieldSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Field
        fields = ('id', 'parentCategory', 'title', 'description', 'created')


class TopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Topic
        fields = ('id', 'parentField', 'title', 'description', 'created')

