from django.apps import AppConfig


class TopicsModuleConfig(AppConfig):
    name = 'topics_module'
