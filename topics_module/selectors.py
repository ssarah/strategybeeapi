from django.shortcuts import render
from topics_module.models import Category, Field, Topic
from topics_module.serializers import CategorySerializer, FieldSerializer, TopicSerializer
from rest_framework import generics
from rest_framework.views import Response
from rest_framework.decorators import permission_classes
from rest_framework.permissions import AllowAny
from new_user_module.models import UserMentor, SbUser
from new_user_module.serializers import UserSerializer, UserMentorSerializer
from new_user_module.selectors import MentorDetailView


# Create your views here.
@permission_classes((AllowAny,))
class CategoryListView(generics.ListAPIView):
    serializer_class = CategorySerializer
    """
    Provides the get method Handler
    """
    def get_queryset(self):
        queryset = Category.objects.all()
        return Category.objects.all()


@permission_classes((AllowAny,))
class FieldListViewSet(generics.ListAPIView):
    serializer_class = FieldSerializer

    def get_queryset(self):
        # 'category_title' is coming from url.py
       parent_category = Category.objects.get(title=self.kwargs['category_title'])
       return Field.objects.filter(parentCategory=parent_category).distinct()


@permission_classes((AllowAny,))
class TopicListViewSet(generics.ListAPIView):
    serializer_class = TopicSerializer

    def get_queryset(self):
        # 'field_title' is coming from url.py
       parent_field = Field.objects.get(title=self.kwargs['field_title'])
       return Topic.objects.filter(parentField=parent_field).distinct()




@permission_classes((AllowAny,))
class AllTopicsViewSet(generics.ListAPIView):
    serializer_class = TopicSerializer

    def get_queryset(self):
        # 'field_title' is coming from url.py

       return Topic.objects.all()


@permission_classes((AllowAny,))
class TopicBasedMentorsList(generics.ListAPIView):
    serializer_class = UserSerializer
    """
    In this function based on the topic_title 'topic_id' will be fetched from Topic table then topic_id will be joined
    as foreign key with UserMentor table and get Mentor_id and sbuser_id then sbuser_id will be used to get whole mentor details.
    """
    def get_queryset(self):

        topic_id = Topic.objects.get(title=self.kwargs['topic_title']).id

        mentor_details = UserMentor.objects.filter(mentoring_topics=topic_id)

        sbuser_id_array = mentor_details.values('sbuser')

        # sbuser_id_in is used to get multiple querysets.
        # return SbUser.objects.filter(sbuserid=sbuser_id).prefetch_related('id__UserMentor_set')
        return SbUser.objects.filter(sbuserid__in=sbuser_id_array)





