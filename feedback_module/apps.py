from django.apps import AppConfig


class FeedbackModuleConfig(AppConfig):
    name = 'feedback_module'
