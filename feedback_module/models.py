from django.db import models
import jsonfield

from meeting_module.models import Meeting


# Create your models here.

class StudentFeedback(models.Model):
    student_feedback_id = models.AutoField(primary_key=True, auto_created=True, serialize=True)
    feedback_questions = jsonfield.JSONField()
    meeting = models.ForeignKey(Meeting, to_field='meeting_id', on_delete=models.CASCADE)


class MentorFeedback(models.Model):
    mentor_feedback_id = models.AutoField(primary_key=True, auto_created=True, serialize=True)
    feedback_questions = jsonfield.JSONField()
    meeting = models.ForeignKey(Meeting, to_field='meeting_id', on_delete=models.CASCADE)






