from django.urls import path
from feedback_module.services import StudentFeedbackView, MentorFeedbackView


urlpatterns =[
    path('student', StudentFeedbackView.as_view()),
    path('mentor', MentorFeedbackView.as_view()),
]