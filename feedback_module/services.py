from feedback_module.serializers import StudentFeedbackSerializer, MentorFeedbackSerializer
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView


# Create your views here.
class StudentFeedbackView(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        student_fb_serializer = StudentFeedbackSerializer(data=request.data)

        if student_fb_serializer.is_valid():
            student_fb_serializer.save()
            return Response(student_fb_serializer.data, status=status.HTTP_201_CREATED)

        return Response(student_fb_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MentorFeedbackView(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        mentor_fb_serializer = MentorFeedbackSerializer(data=request.data)
        if mentor_fb_serializer.is_valid():
            mentor_fb_serializer.save()
            return Response(mentor_fb_serializer.data, status=status.HTTP_201_CREATED)
        return Response(mentor_fb_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
