from feedback_module.models import StudentFeedback, MentorFeedback
from rest_framework import serializers


class StudentFeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentFeedback
        fields = ('student_feedback_id','feedback_questions', 'meeting_id')


class MentorFeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = MentorFeedback
        fields = ('mentor_feedback_id','feedback_questions', 'meeting_id')