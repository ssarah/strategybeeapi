from django.db import models
from new_user_module.models import UserStudent, UserMentor
from topics_module.models import Topic


class Meeting(models.Model):
    #TODO: In the future change meeting_id to uuid- recommend integer id
    meeting_id = models.AutoField(primary_key=True, auto_created=True, serialize=True)
    student = models.ForeignKey(UserStudent, on_delete=models.CASCADE, null=True)
    mentor = models.ForeignKey(UserMentor, on_delete=models.CASCADE, null=True)
    topic =  models.ForeignKey(Topic, to_field='id', on_delete=models.CASCADE, null=True)
    offered_time_slots = models.DateTimeField(null=True)
    scheduled_slot = models.DateTimeField(null=True)
    questions = models.TextField(max_length=1000, null=True)
    payment_status = models.CharField(max_length=10, null=True)
    meeting_status_choice = (
        ('UPCOMING', 'Upcoming'),
        ('CANCELLED', 'Cancelled'),
        ('COMPLETED','Completed'),
        ('DELETED','Deleted'),
    )
    meeting_status = models.CharField(max_length=10, choices=meeting_status_choice, default=None)
    #TODO:payment_id



    class Meta:
        ordering = ('student_id',)

