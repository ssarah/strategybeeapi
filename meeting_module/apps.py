from django.apps import AppConfig


class MeetingModuleConfig(AppConfig):
    name = 'meeting_module'
