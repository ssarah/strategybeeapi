from django.urls import path
from meeting_module.services import CreateMeetingView, \
    MeetingDeleteView
from meeting_module.selectors import MeetingDetailView, StudentMeetingListView, MentorMeetingListView


urlpatterns = [
    path('create',CreateMeetingView.as_view()),
    path('get/<int:meeting_id>', MeetingDetailView.as_view()),
    path('edit/<int:meeting_id>', MeetingDetailView.as_view()),
    path('student_list/<int:student_id>', StudentMeetingListView.as_view()),
    path('mentor_list/<int:mentor_id>', MentorMeetingListView.as_view()),
    # This will change meeting_status
    path('delete/<int:meeting_id>', MeetingDeleteView.as_view()),

    ]

