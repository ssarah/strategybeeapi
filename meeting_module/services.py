from django.shortcuts import render
from django.http import Http404
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView
from meeting_module.serializers import MeetingSerializer, MeetingStatusSerializer
from rest_framework import generics
from meeting_module.models import Meeting


class CreateMeetingView(APIView):
    """
    In this view student_id and mentor_id is coming from existing user information from frontend
    goes in the JSON
    """
    permission_classes = (permissions.AllowAny,)

    # queryset = Meeting.objects.all()
    # serializer_class = MeetingSerializer

    def post(self,request, format=None):
        create_meeting_serializer = MeetingSerializer(data=request.data)
        if create_meeting_serializer.is_valid():
            create_meeting_serializer.save()
            return Response(create_meeting_serializer.data,status=status.HTTP_201_CREATED)
        return Response(create_meeting_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MeetingDeleteView(generics.UpdateAPIView):
    permission_classes = (permissions.AllowAny,)

    def update(self, request, meeting_id, **kwargs):
        data = request.data
        queryset = Meeting.objects.get(meeting_id=meeting_id)
        serializer = MeetingSerializer(queryset, data=data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)








