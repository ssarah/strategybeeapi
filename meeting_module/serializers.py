from meeting_module.models import Meeting
from rest_framework import serializers


class MeetingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Meeting
        fields = ('student','mentor','meeting_id','topic', 'offered_time_slots', 'scheduled_slot',
                  'questions', 'payment_status','meeting_status')


class MeetingStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Meeting
        fields = ('mentor', 'meeting_id',
                  'meeting_status')