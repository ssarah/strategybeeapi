from django.shortcuts import render
from django.http import Http404
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView
from meeting_module.serializers import MeetingSerializer, MeetingStatusSerializer
from rest_framework import generics
from meeting_module.models import Meeting


class MeetingDetailView(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)

    def get_object(self,meeting_id):
        try:
            return Meeting.objects.get(pk=meeting_id)
        except Meeting.DoesNotExist:
            raise Http404

    def get(self, request, meeting_id, format=None):
        meeting = self.get_object(meeting_id)
        serializer = MeetingSerializer(meeting)
        return Response(serializer.data)

    def post(self, request, meeting_id, format=None):

        meeting = self.get_object(meeting_id)
        serializer = MeetingSerializer(meeting, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class StudentMeetingListView(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = MeetingSerializer

    def get_queryset(self):
        return Meeting.objects.filter(student=self.kwargs['student_id']).distinct()


class MentorMeetingListView(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = MeetingSerializer

    def get_queryset(self):
        return Meeting.objects.filter(mentor=self.kwargs['mentor_id']).distinct()
