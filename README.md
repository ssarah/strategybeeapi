# StrategyBeeApi

This app works to provide platform for student and mentor for career strategy making. Using video calling & notes sharing.

Frontend: React REdux HTML CSS

Backend: Python DJango REST Framework, PostgresSQL

Authentication: Google oauth2

Future work: Third party integration

    - stripe for payment gateway
    - video calling gateway

To Run the project:

Install requirements.txt
`pip install requirements.txt`

Activate Environment ven40

In root folder Run
`python manage.py runserver`
