def handle_uploaded_file(each_file):
    with open(each_file.name, 'wb+') as destination:
        for chunk in each_file.chunks():
            destination.write(chunk)