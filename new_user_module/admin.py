from django.contrib import admin
from .models import SbUser, UserStudent, UserMentor

# Register your models here.
admin.site.register(SbUser)
admin.site.register(UserStudent)
admin.site.register(UserMentor)
