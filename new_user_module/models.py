from django.db import models
import uuid
from django.core.validators import FileExtensionValidator
import datetime

from topics_module.models import Topic


def upload_to(instance, filename):
    return 'display_picture_images/{filename}'.format(filename=filename)


class SbUser(models.Model):

    gender_choice = (('Male', 'Male'), ('Female', 'Female'),)
    student_mentor_choice = (('Student', 'Student'), ('Mentor', 'Mentor'),)

    sbuserid = models.UUIDField(primary_key=True, db_index=True, default=uuid.uuid4, editable=False)
    google_id = models.CharField(max_length=100, blank=True)
    first_name = models.CharField(max_length=100, blank=False)
    last_name = models.CharField(max_length=100, blank=False)
    email = models.EmailField(blank=False)
    education = models.CharField(max_length=100, blank=True)
    user_type = models.CharField(max_length=10, choices=student_mentor_choice)
    gender = models.CharField(max_length=10, choices=gender_choice)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    user_location = models.CharField(max_length=100, blank=True)
    display_picture = models.ImageField(upload_to=upload_to, blank=True, null=True, default=None)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'email']


class UserStudent(models.Model):
    studentPaymentChoice = (('Card', 'Card'), ('Paypal', 'Paypal'),)
    id = models.UUIDField(primary_key=True, db_index=True, default=uuid.uuid4, editable=False)
    sbuser = models.ForeignKey(SbUser, on_delete=models.CASCADE, default=uuid.uuid4)
    student_description = models.TextField(max_length=1000, blank=False, default="")
    student_payment_method = models.CharField(max_length=10, choices=studentPaymentChoice, default="")


class UserMentor(models.Model):
    mentor_payment_method_choice = (('Bank', 'Bank'), ('Paypal','Paypal'),)
    mentoring_mode_choice = (('VideoCall', 'VideoCall'), ('AudioCall', 'AudioCall'),)

    id = models.UUIDField(primary_key=True, db_index=True, default=uuid.uuid4, editable=False)
    sbuser = models.ForeignKey(SbUser, on_delete=models.CASCADE, default=uuid.uuid4)
    portfolio_link = models.CharField(max_length=100, blank=True)
    mentor_description = models.TextField(max_length=1000, default="", blank=True)
    mentor_payment_method = models.CharField(max_length=10, choices=mentor_payment_method_choice)
    mentoring_mode = models.CharField(max_length=10, choices=mentoring_mode_choice)
    mentoring_topics = models.ManyToManyField(Topic, related_name='mentor_topics')
    verified_mentor = models.BooleanField(default=False)


class MentorMedia(models.Model):
    intro = models.FileField(upload_to='media/intro', blank=True, default=None)
    verification_id = models.ImageField(upload_to='media/mentor_verification_id', blank=True, default=None)
    validators = [FileExtensionValidator(allowed_extensions=['MOV','avi','mp4','webm','mkv'])]
    mentor = models.ForeignKey(UserMentor, on_delete=models.CASCADE, default=uuid.uuid4)


class MentorAvailability(models.Model):
    id = models.UUIDField(primary_key=True, db_index=True, default=uuid.uuid4, editable=False)
    available_date = models.DateField()
    available_day = models.CharField(max_length=50)
    start_time = models.TimeField()
    end_time = models.TimeField()
    mentor = models.ForeignKey(UserMentor, on_delete=models.CASCADE, default=uuid.uuid4)
    updated_on = models.DateTimeField(blank=True, null=True, default=datetime.date.today)



