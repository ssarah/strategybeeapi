from django.http import Http404
from rest_framework import generics
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from new_user_module.serializers import UserSerializer, UserMentorSerializer
from new_user_module.models import SbUser, UserMentor, UserStudent


# Create your views here.
class UsersProfileListView(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = UserSerializer

    def get_queryset(self):
        return SbUser.objects.filter(is_active__exact=True)


class UsersMentorListView(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = UserMentorSerializer

    def get_queryset(self):
        return UserMentor.objects.all()


class MentorDetailView(generics.ListAPIView):

    permission_classes = (permissions.AllowAny,)
    serializer_class = UserSerializer

    def get_queryset(self):
        mentor_details =UserMentor.objects.filter(id= self.kwargs['mentor_id'])
        sbuser_id = mentor_details.values('sbuser')[0]['sbuser']
        # return SbUser.objects.filter(sbuser_id=sbuser_id).prefetch_related('sbuser_id__UserMentor_set')
        return SbUser.objects.filter(sbuserid=sbuser_id)


class StudentDetailView(generics.ListAPIView):
    """
    Provides the get method Handler
    """
    permission_classes = (permissions.AllowAny,)
    serializer_class = UserSerializer

    def get_queryset(self):
        student_details =UserStudent.objects.filter(id= self.kwargs['student_id'])
        sbuser_id = student_details.values('sbuser')[0]['sbuser']
        # return SbUser.objects.filter(sbuserid=sbuser_id).prefetch_related('sbuser_id__UserStudent_set')
        #TODO: Figure out the use of prefetch_related here
        return SbUser.objects.filter(sbuserid=sbuser_id).prefetch_related('usermentor_set')


class ProfileDetailView(APIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = UserSerializer

    def get_object(self, sbuserid):
        try:
            return SbUser.objects.get(sbuserid=sbuserid)
        except SbUser.DoesNotExist:
            raise Http404

    def get(self, request, sbuserid):
        SbUser = self.get_object(sbuserid)
        serializer = UserSerializer(SbUser)
        if serializer.data['is_active']:
            return Response(serializer.data)
        else:
            return Response({"users": "No Users Found!"}, status=status.HTTP_204_NO_CONTENT)

    def delete(self, request, sbuserid):
        user_profile = self.get_object(sbuserid=sbuserid)
        user_profile.is_active = False
        user_profile.save()
        return Response({"users": "No Users Found!"}, status=status.HTTP_204_NO_CONTENT)
