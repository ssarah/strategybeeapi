from django.urls import path
from new_user_module.services import CreateProfileView,  UserMediaView
from new_user_module.selectors import UsersProfileListView, UsersMentorListView, MentorDetailView, ProfileDetailView, StudentDetailView



urlpatterns = [

    path('profile/list', UsersProfileListView.as_view()),
    path('profile/<uuid:sbuserid>', CreateProfileView.as_view()),
    path('profile/create', CreateProfileView.as_view()),
    # path('profile/create/media', UserMediaView.as_view()),
    path('profile/get/<uuid:sbuserid>', ProfileDetailView.as_view()),
    path('student_profile/get/<uuid:student_id>', StudentDetailView.as_view()),
    path('mentor_profile/get/<uuid:mentor_id>', MentorDetailView.as_view()),
    path('mentors/list', UsersMentorListView.as_view()),
    path('profile/delete/<uuid:sbuserid>', ProfileDetailView.as_view()),

]