from django.apps import AppConfig


class NewUserModuleConfig(AppConfig):
    name = 'new_user_module'
