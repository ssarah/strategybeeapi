from new_user_module.models import SbUser, UserStudent, UserMentor, MentorMedia, MentorAvailability
from rest_framework import serializers


class MentorMediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = MentorMedia
        fields = ('id','intro','verification_id','mentor')


class MentorAvailabilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = MentorAvailability
        fields = ('id','available_date','available_day','start_time','end_time','mentor','updated_on')


class UserMentorSerializer(serializers.ModelSerializer):
    mentor_media = MentorMediaSerializer(many=True, read_only=True,source="mentormedia_set" )
    mentor_availability = MentorAvailabilitySerializer(many=True, read_only=True, source="mentoravailability_set")

    class Meta:
        model = UserMentor
        fields = ('id','sbuser','portfolio_link','mentor_description','mentor_payment_method','mentoring_topics',
                  'mentoring_mode','verified_mentor','mentor_media','mentor_availability')


class UserStudentSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserStudent
        fields = ('id','sbuser','student_description','student_payment_method')


class UserSerializer(serializers.ModelSerializer):
    # source is included so that user_student and user_mentor gets serialized
    display_picture = serializers.ImageField(required=False)
    # user_student = UserStudentSerializer(many=True, read_only=True, source='userstudent_set')
    # user_mentor = UserMentorSerializer(many=True, read_only=True, source='usermentor_set')


    class Meta:
        model = SbUser
        fields = ('sbuserid', 'google_id','first_name', 'last_name', 'email', 'education', 'user_type', 'gender',
                  'is_active', 'is_admin', 'user_location','display_picture')
        depth = 1


