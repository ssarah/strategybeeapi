import ast
import json
from rest_framework import permissions, status
from rest_framework.exceptions import ParseError
from rest_framework.parsers import MultiPartParser, FormParser, JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser
from rest_framework.decorators import parser_classes
from .models import SbUser

from new_user_module.serializers import UserSerializer, \
    UserStudentSerializer, UserMentorSerializer, MentorMediaSerializer, MentorAvailabilitySerializer
# from .utility import handle_uploaded_file


# TODO: Fix backend to receive POST request from Frontend for create user profile with json and media two seperate
#  requests.
class CreateProfileView(APIView):
    """
    Created Profile by assuming "is_active is always True"
    """
    permission_classes = (permissions.AllowAny,)
    parser_classes = (JSONParser, MultiPartParser, FormParser)

    def post(self, request):

        user_data = json.loads(request.data["user"])
        user_serializer = UserSerializer(data=user_data)

        if user_serializer.is_valid():
            user_serializer.save()
            user_type = user_data["user_type"]
            sbuserid = user_serializer.data['sbuserid']

            if user_type == "Mentor":
                user_mentor_data = json.loads(request.data["user_mentor"])
                user_mentor_data['sbuser'] = sbuserid
                mentor_serializer = UserMentorSerializer(data=user_mentor_data)
                if mentor_serializer.is_valid(raise_exception=True):
                    mentor_serializer.save()

                    user_serializer.data['user_mentor'].append(mentor_serializer.data)
                    return Response({"status": "success", "data": user_serializer.data,},
                                status=status.HTTP_201_CREATED)

                return Response({"status": "error", "data": user_serializer.data,
                             "mentor_data": mentor_serializer.error_messages},
                            status=status.HTTP_400_BAD_REQUEST)

            elif user_type == "Student":
                user_student_data = json.loads(request.data["user_student"])
                # student_data = user_data["user_student"]

                user_student_data['sbuser'] = sbuserid
                student_serializer = UserStudentSerializer(data=user_student_data)
                if student_serializer.is_valid(raise_exception=True):
                    student_serializer.save()
                    return Response({"status": "success"},
                                    status=status.HTTP_201_CREATED)

                return Response({"status": "error",
                                 "mentor_data": student_serializer.errors},
                                status=status.HTTP_400_BAD_REQUEST)

        else:
            return Response({"status": "error", "data": user_serializer.errors},
                            status=status.HTTP_400_BAD_REQUEST)


    def put(self, request, sbuserid):
        parser_classes = (MultiPartParser, FormParser)
        user_profile = SbUser.objects.get(sbuserid=sbuserid)
        user_serializer = UserSerializer(data=user_profile)
        try:
            # display_picture = request.FILES['user[0].display_picture']
            display_picture = request.FILES
        except KeyError:
            raise ParseError('Request has no image')
        user_serializer.update(validated_data=display_picture, instance=user_profile)
        if user_serializer.is_valid():
            user_serializer.save()
        return Response({"status": "Success", "data":user_serializer.data})



# @parser_classes((MultiPartParser,))
class UserMediaView(APIView):
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request):
        # add display pic from second request using auth and sbuserid
        user_data = json.loads(request.data["user"])
        mentor_media_data = json.loads(request.data["user_mentor"])

        google_id = user_data["google_id"]
        display_picture = user_data["display_picture"]
        user_profile = SbUser.objects.get(google_id=google_id)
        user_profile.display_picture = display_picture
        user_profile.save()

        if user_data["user_type"] == "Mentor":
            mentor_media_serializer = MentorMediaSerializer(data=mentor_media_data)
            if mentor_media_serializer.is_valid():
                mentor_media_serializer.save()
                return Response({"status": "Success", status:status.HTTP_201_CREATED})
            else:
                return Response({"status": "Error", status:status.HTTP_400_BAD_REQUEST})

        elif user_data["user_type"] == "Student":
            return Response({"status": "Success", status:status.HTTP_201_CREATED})


